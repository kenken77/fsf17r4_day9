/**
 * Client side code.
 */
(function () {
    "use strict";
    angular.module("RegApp", ['ngAnimate']);
})();