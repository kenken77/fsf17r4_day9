(function () {
    "use strict";
    angular.module("RegApp")
        .controller("RegCtrl", RegCtrl)
        .controller("RegisteredCtrl", RegisteredCtrl);
    
    // custom service ' and angular service ecan be ' or "
    RegCtrl.$inject = ['RegServiceAPI',"$log"]; 
    RegisteredCtrl.$inject = ['RegServiceAPI',"$log"];

    function RegisteredCtrl(RegServiceAPI,  $log){
        var self = this;
        console.log(">>> " + RegServiceAPI.getFullname());
        self.fullname = RegServiceAPI.getFullname();
    }

    function RegCtrl(RegServiceAPI,  $log) {
        var regCtrlself  = this;
        
        regCtrlself.onSubmit = onSubmit;
        regCtrlself.initForm = initForm;
        regCtrlself.onReset = onReset;
        
        regCtrlself.onlyFemale = onlyFemale;
        regCtrlself.isSubmitted = false;

        regCtrlself.emailFormat = /^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/;
        regCtrlself.user = {
            
        }

        regCtrlself.nationalities = [
            { name: "Please select" , value:0},
            { name: "Singaporean", value: 1},
            { name: "Indonesian", value: 2},
            { name: "Thai", value: 3},
            { name: "Malaysian", value: 4},
            { name: "Australian", value: 5}      
        ];

        function initForm(){
            regCtrlself.user.selectedNationality = "0";
            regCtrlself.user.gender = "F";
            regCtrlself.isSubmitted = false;
        }

        function onReset(){
            regCtrlself.user = Object.assign({}, regCtrlself.user);
            regCtrlself.registrationform.$setPristine();
            regCtrlself.registrationform.$setUntouched();
        }

        function onSubmit(){
            console.log(RegServiceAPI);
            RegServiceAPI.register(regCtrlself.user)
                .then((result)=>{
                    regCtrlself.user = result;
                    console.log($log);
                    $log.info(result);
                    regCtrlself.isSubmitted = true
                }).catch((error)=>{
                    console.log(error);
                })
        }

        function onlyFemale(){
            return regCtrlself.user.gender == "F";
        }

        regCtrlself.initForm();
    }
    
})();    